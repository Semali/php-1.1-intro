<?php
$name = 'Сергей';
$age = 26;
$email = 'semali1989@gmail.com';
$city = 'Москва';
$comment = 'Ученик Нетологии';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><?= $name ?> - <?= $comment ?></title>
		<style>
			body {
				font-family: sans-serif;
			}
			dl {
				display: table-row;
			}
			dt, dd {
				display: table-cell;
				padding: 5px 10px;
			}
		</style>
	</head>
	<body>
		<h1>Страница пользователя <?= $name ?></h1>
		<dl>
			<dt>Имя</dt>
			<dd><?= $name ?></dd>
		</dl>
		<dl>
			<dt>Возраст</dt>
			<dd><?= $age ?></dd>
		</dl>
		<dl>
			<dt>Адрес электронной почты</dt>
			<dd><a href="<?= $email ?>"><?= $email ?></a></dd>
		</dl>
		<dl>
			<dt>Город</dt>
			<dd><?= $city ?></dd>
		</dl>
		<dl>
			<dt>О себе</dt>
			<dd><?= $comment ?></dd>
		</dl>
	</body>
</html>
